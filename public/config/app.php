<?php

return [
    'providers' => [
        'routing' => \app\Providers\Routing\RoutingProvider::class,
        'ORM' => \app\Providers\ORMServiceProvider::class,
    ],
];