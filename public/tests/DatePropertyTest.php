<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use app\ORM\Property\DateProperty;


/**
 * Тест класс DatePropertyTest
 *
 * @uses TestCase
 * @package app/tests
 * @category Test
 * @return void
 */
class DatePropertyTest extends TestCase
{
    protected $dateProperty;

    /**
     *Установка глобального объекта для класса
     */
    protected function setUp()
    {
        $this->dateProperty = new DateProperty;
    }


    /**
     *Проверка метода Check
     */
    public function testCheck()
    {
        $result =  $this->dateProperty->check('2014-02-04') ;

        $this->assertTrue($result);

    }

    /**
     *Проверка метода GetFormattedValue
     */
    public function testGetFormattedValue()
    {
        $result =  $this->dateProperty->getFormattedValue('20-02-2014') ;

        $this->assertEquals('2014-02-20', $result);

    }

    /**
     *Удаление ссылок на все глобальные объекты
     */
    protected function tearDown()
    {
        $this->dateProperty = NULL;
    }

    /**
     * @depends testCheck
     * @depends testGetFormattedValue
     */
    public function testGetFormated()
    {
         $this->dateProperty->val = '04-02-2018';

        $this->assertEquals('2018-02-04', $this->dateProperty->value);

    }

    /**
     * @depends testCheck
     * @depends testGetFormattedValue
     */
    public function testGetChecked()
    {
        $this->dateProperty->val = '2018-02-04';

        $this->assertEquals('2018-02-04', $this->dateProperty->value);

    }



}