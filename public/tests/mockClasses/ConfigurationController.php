<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 27.04.18
 * Time: 23:52
 */

namespace Tests\mockClasses;
use app\Components\Configuration\ConfigurationInterface;

class ConfigurationController implements ConfigurationInterface
{
    public $configs;


    function __get($name)
    {

        return [
            'config' =>
            [
                'connect1' => [
                    'type' => 'mysql',
                    'host' => 'localhost',
                    'dbuser' => 'root',
                    'dbpass' => '',
                    'dbname' => 'test',
                ],

             ]
            ];

    }

}