<?php

namespace Tests;

use app\ORM\Driver\Driver;
use PHPUnit\Framework\TestCase;
use app\Providers\ORMServiceProvider;
use Tests\mockClasses\ConfigurationController;

/**
 * Тест класс ORMServiceProviderTest
 *
 * @uses TestCase
 * @package app/tests
 * @category Test
 * @return void
 */
class ORMServiceProviderTest extends TestCase
{
    public $configuration;

    public function testBoot()
    {

        $this->configuration = new ConfigurationController;
        $serviceProvider = new ORMServiceProvider;
        $serviceProvider->setConfiguration($this->configuration);

        $result = $serviceProvider->boot();

        $this->assertInstanceOf(
            Driver::class,
            $result
        );

    }

}
