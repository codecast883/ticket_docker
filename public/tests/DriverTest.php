<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use app\ORM\Driver\Driver;


/**
 * Тест класс DriverTest
 *
 * @uses TestCase
 * @package app/tests
 * @category Test
 * @return void
 */
class DriverTest extends TestCase
{
    private $driver;


    /**
     * Тест на массив
     *
     * @return bool
     */
    public function testGetAvailableDriversArray()
    {
        $array = Driver::getAvailableDrivers();
        $this->assertInternalType('array', $array);

    }


    public function data_getAvailableDrivers_DriverClasses(){
        $result = [];

        foreach (Driver::getAvailableDrivers() as $type => $class){
            $result[] = [
                $type,
                $class
            ];
        }

        return $result;
    }

    /**
     * Тест на структуру
     *
     * @dataProvider data_getAvailableDrivers_DriverClasses
     */
    public function testGetAvailableDriversStructure($type, $class)
    {
        $this->assertInstanceOf(
            Driver::class,
            new $class
        );
    }

    /**
     * Тест на структуру
     *
     * @dataProvider data_getAvailableDrivers_DriverClasses
     */
    public function test_getAvailableDriversStructure($type, $class)
    {
        $this->assertEquals(
            $class::getType(),
            $type
        );
    }

}