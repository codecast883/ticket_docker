<?php

namespace app;

use app\Components\Configuration\ConfigurationController;
use app\ORM\Model;
use app\ORM\PropertyCollection;

class App
{

   public static $instance;
    public $appConfig;

    public $providers = [];

    public function __construct()
    {

       $this->appConfig = (new ConfigurationController)->App;

    }

    private function boot()
    {
        $this->registerProviders();
    }

    public function ORM(){
        return new PropertyCollection();
    }

    private function registerProviders(){

        foreach ($this->appConfig->providers as $name => $provider){
            $className = '\\'.$provider;

            $this->providers[$name] = (new $className)->boot();

        }
    }

    public function run()
    {
        $this->boot();

//        $this->ORM();
//        $this->register();
//
//        $this->providers['Route']->exec;
//
    }

    private function register()
    {
        foreach ($this->appConfig->providers as $name => $provider){
            $className = '\\'.$provider;
            $this->providers[$name] = (new $className)->register();
        }

    }

}