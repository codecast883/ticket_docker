<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 15.04.18
 * Time: 22:18
 */

namespace app\ORM;

class PropertyCollection
{
    public $properties = [];
    public $propertiesStorage = [];

    public function __construct()
    {
        $this->init();
    }

    public function init(){
        $propertyDirectory = HOME_DIR . '/app/ORM/Property';
        $dir = scandir($propertyDirectory);
        $className = '';
        foreach ($dir as $value){
            if (preg_match('/.php/',$value)){
                $className = "app\ORM\Property\\" . strstr($value, '.php',true);
                try {
                    $class = new \ReflectionClass($className);

                    if (($class->getInterfaceNames()) == 'PropertyInterface') {
                        $type = '';
                        if ($type = $className::getType()) {
                            $this->properties[$type] = $className;
                        }
                    }

                } catch (\app\Components\Exceptions\ORMException $e) {

                    echo 'Ошибка: ',  $e->getMessage(), "\n";
                }
            }
        }
    }

    public function &__get($name)
    {
        $this->propertiesStorage[$name]->getValue();
    }


    public function __set($name, $value)
    {
        $this->propertiesStorage[$name] = $value;
    }

    public function makeProperties($type,$code)
    {
        if (!array_key_exists($type, $this->properties))
            throw new \app\Components\Exceptions\ORMException('Запрошенный тип св-ва не найден');

        $className = $this->properties[$type];
        $this->propertiesStorage[$code] = new $className();
    }


}