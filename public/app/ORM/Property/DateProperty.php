<?php
/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 19.04.18
 * Time: 9:45
 */

namespace app\ORM\Property;

use app\ORM\Property;

class DateProperty extends Property
{

    public $value;
    public static $type = 'Date';

    public static function getType()
    {
        return static::$type;

    }

    public function getFormattedValue($value)
    {

        $date = new \DateTime($value);
        return $date->format('Y-m-d');

    }


    public function check($value)
    {
        $pattern = "/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/";

        if (preg_match($pattern, $value)) {
            return true;
        }

        return false;
    }


    public function __get($value)
    {
        if (method_exists($this, 'getFormattedValue')) {

            return $this->value;
        }

    }

    public function __set($name, $value)
    {

        if (!property_exists($this, $name)) {

            if ($this->check($value)) {
                $this->value = $value;
            } else {
                $this->value = $this->getFormattedValue($value);
            }

        }

    }


}