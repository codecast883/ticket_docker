<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 15.04.18
 * Time: 22:14
 */

namespace app\ORM\Property;
use app\ORM\Property;

class IntegerProperty extends Property
{
    public $value;
    public static $type = 'Integer';

    public static function getType()
    {
        return static::$type;
    }



}