<?php
/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 19.04.18
 * Time: 9:51
 */

namespace app\ORM\Property;


class DatetimeProperty extends Property
{

    public $value;
    public static $type = 'Date';

    public static function getType()
    {
        return static::$type;
    }

    public function getFormattedValue()
    {
        if (!empty($value)){
            $date = new \DateTime($this->value);
            return $date->format('Y-m-d H:i:sP');
        }

    }

}
