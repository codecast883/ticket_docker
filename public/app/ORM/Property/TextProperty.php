<?php
/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 19.04.18
 * Time: 9:54
 */

namespace app\ORM\Property;


class TextProperty extends Property
{

    public $value;
    public static $type = 'Text';

    public static function getType()
    {
        return static::$type;
    }

    public function getFormattedValue(){}

}