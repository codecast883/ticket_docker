<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 15.04.18
 * Time: 22:15
 */

namespace app\ORM\Property;
use app\ORM\Property;

class VarcharProperty extends Property
{
    public $type = 'Varchar';

    public function getType()
    {
        return $this->type;
    }



}