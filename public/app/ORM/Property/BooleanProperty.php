<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 15.04.18
 * Time: 22:15
 */


namespace app\ORM\Property;
use app\ORM\Property;

class BooleanProperty extends Property
{
    public $value;
    public static $type = 'Boolean';

    public static function getType()
    {
        return static::$type;
    }

    public function getFormattedValue(){}



}