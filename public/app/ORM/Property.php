<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 17.04.18
 * Time: 8:50
 */

namespace app\ORM;


/**
 * Абстрактный класс Property
 *
 * Данный класс является базовым для всех свойств
 * из коллекции и реализует интерфейс PropertyInterface
 *
 * @package app\Providers
 * @category Providers
 * @return void
 */
abstract class Property implements PropertyInterface
{
    public $value;
    public static $type;


    /**
     * Метод сеттер
     *
     * @param $property
     * @param $value
     */
    public function __set($property, $value)
    {
        // TODO: Implement __set() method.
    }

    /**
     * Метод геттер
     *
     * @param $name
     */
    public function __get($name)
    {
        // TODO: Implement __get() method.
    }


}