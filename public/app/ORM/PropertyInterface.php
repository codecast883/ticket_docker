<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 15.04.18
 * Time: 22:12
 */

namespace app\ORM;


interface PropertyInterface
{
    public static function getType();
    public function __get($value);
    public function __set($property, $value);
    public function getFormattedValue($value);
    public function check($value);

}