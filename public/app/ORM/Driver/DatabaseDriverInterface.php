<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 23.04.18
 * Time: 23:46
 */

namespace app\ORM\Driver;


interface DatabaseDriverInterface
{

    public static function getType();

    public function query($sql);

    public function connect();

    public function disconnect();

    public function fetchResult($sql);

}