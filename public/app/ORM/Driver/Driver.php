<?php
namespace app\ORM\Driver;

/**
 * Абстрактный класс Driver
 *
 * Данный класс наследуют все типы классов драйверов
 *
 * @package app\ORM\Driver
 * @category Drivers
 * @return void
 */
 abstract class Driver
{

     /**
      * Вывод всех доступных классов драйверов, зарегестрированных в методе
      *
      * @return array
      */
    static function getAvailableDrivers(){

        return [
            'mysql' => MysqlDriver::class,
            'mysq2' => MysqlDriver::class,
            'mysq4' => MysqlDriver::class,
        ];

    }


}