<?php
/**
 * Created by PhpStorm.
 * User: windsweb
 * Date: 24.04.18
 * Time: 0:05
 */

namespace app\ORM\Driver;


class MysqlDriver extends Driver implements DatabaseDriverInterface
{

    private static $type = 'mysql';

    protected $dbh;

    public function connect()
    {

       if (!$this->dbh = new \mysqli($config['host'], $config['dbuser'], $config['dbpass'], $config['dbname'])) {

           throw new \app\Components\Exceptions\ORMException('Ошибка подключения к базе данных');
       }
    }

    public function query($sql)
    {
        if (!$result =$this->dbh->query($sql)){
            throw new \app\Components\Exceptions\ORMException('Не удалось сделать запись:' .  $result->error);
        }

        return $result;
    }

    public static function getType()
    {
        return self::$type;
    }

    public function fetchResult($sql)
    {
        $result = $this->query($sql)->fetch_assoc();
        if (!$result){
            throw new \app\Components\Exceptions\ORMException('Не удалось ивлечь данные:' .  $result->error);
        }

      return $result;
    }

    public function disconnect()
    {
        // TODO: Implement disconnect() method.
    }

}