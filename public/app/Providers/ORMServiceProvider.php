<?php
namespace app\Providers;
use app\Components\Configuration\ConfigurationInterface;
use app\Components\ServiceProvider\ServiceProvider;
use app\Components\Configuration\ConfigurationController;
use app\ORM\Driver\Driver;

/**
 * Класс ORMServiceProvide
 *
 * Данный класс определяет конфигурацию ORM
 * и регистрирует нужные драйвера для дальнейшей работы
 *
 * @package app\Providers
 * @category Providers
 * @return void
 */

class ORMServiceProvider extends ServiceProvider
{

    public $config;
    public static $driver;


    /**
     * Метод boot сервис-провайдера
     *
     * @uses ConfigurationController
     * @uses Driver
     * @return mixed
     */
    public function boot(){

        $configs = $this->setConfiguration($this->config)->Db;

        foreach ($configs->config as $connection=>$conf){

            $drivers = Driver::getAvailableDrivers();

            self::$driver = new $drivers[$conf['type']];

            return self::$driver;
        }

    }

    /**
     * Метод-фабрика сервис-провайдера
     *
     * Принимает класс конфигурации реализцющий ConfigurationInterface
     *
     * @uses ConfigurationInterface
     * @param ConfigurationInterface $configuration
     */

    public function setConfiguration(ConfigurationInterface $configuration){
        $this->config = new $configuration;
    }


    public function register(){

    }

}