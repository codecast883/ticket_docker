<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 02.04.2018
 * Time: 0:29
 */

namespace app\Providers\Routing;


class Route
{
    protected static $url;
    protected static $requestedMethod;
    protected static $routing;
    public static $currentRoutings = [];


    public function exec()
    {


        foreach (self::$currentRoutings as $value) {

            return (new Routing($value['method'],$value['link'],$value['callback']))->exec();

        }

    }



    static function get(string $linkPattern,callable $callback)
    {

        self::$currentRoutings[] = [
            'link' => $linkPattern,
            'callback' => $callback,
            'method' => 'get',
        ];

    }

    static function post(string $linkPattern,callable $callback)
    {
        self::$currentRoutings[] = [
            'link' => $linkPattern,
            'callback' => $callback,
            'method' => 'post',
        ];
    }

    static function any(string $linkPattern,callable $callback)
    {
        self::$currentRoutings[] = [
            'link' => $linkPattern,
            'callback' => $callback,
            'method' => 'any',
        ];
    }


}