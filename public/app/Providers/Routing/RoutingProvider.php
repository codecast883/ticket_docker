<?php
namespace app\Providers\Routing;

use app\Components\ServiceProvider\ServiceProvider;
use app\Providers\Routing\Route;
use app\Providers\Routing\Config;
class RoutingProvider extends ServiceProvider
{

    public static $routes;

    public function boot(){
        return new Route();


    }

    public function register(){

        self::$routes =(new Config)->app;
        return self::$routes;

    }


}