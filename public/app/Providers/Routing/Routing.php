<?php
/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 03.04.18
 * Time: 9:49
 */

namespace app\Providers\Routing;


class Routing
{
    public $requestType;
    public $linkPattern;
    public $callback;

    public function __construct($requestType,$linkPattern,$callback)
    {
        $this->requestType = $requestType;
        $this->linkPattern = $linkPattern;
        $this->callback = $callback;


    }


    private function getUri()
    {

        $url = trim(htmlspecialchars($_SERVER['REQUEST_URI']), '/');
        if (!empty($url)) {
            $uriArray = explode('?', $url);
            return array_shift($uriArray);
        }

    }

    private function requestMethod()
    {

        return $_SERVER['REQUEST_METHOD'];

    }

    public function preg(){

        $uri = $this->getUri();
        $requestMethod = $this->requestMethod();

        if (preg_match("~^ $uri+$~", $this->linkPattern) and $this->requestType == $requestMethod) {

            if (strpos($this->callback, '/')) {
                $controllerArray = explode('/', $this->callback);
                $controller = $controllerArray[0];
                $method = $controllerArray[1];

                $controllerName = $controller . 'Controller';
                $controllerName = ucfirst($controllerName);

                return new $controllerName->$method;
            }
            return $this->callback;

        }


    }

    public function exec(){
        return $this->preg();

    }

}