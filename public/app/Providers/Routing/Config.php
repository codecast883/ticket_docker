<?php
/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 03.04.18
 * Time: 12:03
 */

namespace app\Providers\Routing;


class Config
{
    public static $routesConfigContainer = [

        'app' => HOME_DIR . "routes/Routes.php",
        'api' => HOME_DIR . "routes/api.php",

    ];


    function  __get($name)
    {
        if (!empty(self::$routesConfigContainer[$name])){
            return self::$routesConfigContainer[$name];
        }else{
            throw new \Exception('Missing: Routes is not defined');
        }

    }
}