<?php

namespace app\Components\ServiceProvider;

interface ServiceProviderInterface
{
    public function boot();
    public function register();
}