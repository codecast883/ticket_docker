<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 01.04.2018
 * Time: 21:57
 */

namespace app\Components\Configuration;


use Composer\Config\ConfigSourceInterface;

class ConfigurationController implements ConfigurationInterface
{
    public $configs;


    function __get($name)
    {
        $className = '\app\Components\Configuration\\'.$name.'Configuration';

        if ($class = new $className){
            return $class;
        }else{
            throw new \Exception('Ошибка, конфигурация не опредлена');
        }



    }

}