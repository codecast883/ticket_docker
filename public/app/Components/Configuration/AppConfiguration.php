<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 01.04.2018
 * Time: 20:22
 */

namespace app\Components\Configuration;


class AppConfiguration extends Configuration
{
    public $configPath = HOME_DIR . 'config/app.php';

}