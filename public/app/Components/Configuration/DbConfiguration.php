<?php
/**
 * Created by PhpStorm.
 * User: Yaroslav
 * Date: 01.04.2018
 * Time: 20:33
 */

namespace app\Components\Configuration;


class DbConfiguration extends Configuration
{
    public $configPath = HOME_DIR . 'config/db.php';
}