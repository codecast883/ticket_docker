<?php
/**
 * Created by PhpStorm.
 * User: yaroslav
 * Date: 28.04.18
 * Time: 11:19
 */

namespace app\Components\Configuration;


interface ConfigurationInterface
{

    public function __get($name);


}