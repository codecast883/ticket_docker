<?php


namespace app\Components\Configuration;

abstract class  Configuration
{
    public $config;
    protected $configPath;

    public function __construct()
    {
        $this->loadConfig();
    }
    protected function loadConfig()
    {
        $this->config = require_once($this->configPath);
    }

    protected function getValue($value)
    {
        return $this->config[$value];
    }

    function __get($name)
    {
        if (!empty($this->getValue($name))){
            return $this->getValue($name);
        }else{
            throw new \Exception('Конфигурация не найдена');
        }



    }

    public function getFirst(){
        return $this->config[0];
    }
}