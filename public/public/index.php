<?php
require_once ("../vendor/autoload.php");
require_once ("../app/App.php");

use app\App;

define ("HOME_DIR", __DIR__ . '/../');


$application = new App();

$application->run();
