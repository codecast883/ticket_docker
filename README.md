# Simple Docker LEMP stack for local dev
Based on tutorial here: [http://www.penta-code.com/creating-a-lemp-stack-with-docker-compose/](http://www.penta-code.com/creating-a-lemp-stack-with-docker-compose/).

Docker image consists of:
- nginx
- php 7
- mysql
- phpmyadmin


## Requirements
- docker
- docker-compose

## First-time setup
Be sure to add the local log files.

```
touch logs/nginx-access.log logs/nginx-error.log
```

## Starting the containers
```docker-compose up -d```

To check that it's running: [http://localhost:8080](http://localhost:8080)

## Accessing MySql in shell
```
docker-compose exec mysql sh
mysql -u root -p
```
(mysql password is admin)

## Accessing phpmyadmin
- Url: [http://localhost:8183](http://localhost:8183)
- Server: mysql
- User: root
- Password: admin
